package com.desafio_android.network

import com.concrete_challenge.datas.GitHubResponse
import com.concrete_challenge.datas.PullRequest
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by jackmiras on 11/29/17.
 */
interface ApiService {

    @GET("search/repositories?q=language:Java&sort=stars")
    fun fetchRepositories(
        @Query("page") page: Int,
        @Query("per_page") perPage: Int): Observable<GitHubResponse>

    @GET("repos/{creator}/{repository}/pulls")
    fun fetchPullRequests(
        @Path("creator") creator: String,
        @Path("repository") repository: String): Observable<List<PullRequest>>
}