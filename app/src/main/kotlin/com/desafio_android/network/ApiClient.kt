package com.desafio_android.network

import com.desafio_android.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

/**
 * Created by jackmiras on 11/29/17.
 */
object ApiClient {

    val apiService: ApiService by lazy {
        Retrofit.Builder()
            .baseUrl(BuildConfig.DOMAIN)
            .client(getHttpClient())
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    private fun getHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor().setLevel(BODY)
        return OkHttpClient().newBuilder().addInterceptor(interceptor).build()
    }
}