package com.concrete_challenge.datas

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

data class User(

        @field:JsonProperty("id")
        val id: Int = 0,

        @field:JsonProperty("gists_url")
        val gistsUrl: String = "",

        @field:JsonProperty("repos_url")
        val reposUrl: String = "",

        @field:JsonProperty("following_url")
        val followingUrl: String = "",

        @field:JsonProperty("starred_url")
        val starredUrl: String = "",

        @field:JsonProperty("login")
        val login: String = "",

        @field:JsonProperty("followers_url")
        val followersUrl: String = "",

        @field:JsonProperty("type")
        val type: String = "",

        @field:JsonProperty("url")
        val url: String = "",

        @field:JsonProperty("subscriptions_url")
        val subscriptionsUrl: String = "",

        @field:JsonProperty("received_events_url")
        val receivedEventsUrl: String = "",

        @field:JsonProperty("avatar_url")
        val avatarUrl: String = "",

        @field:JsonProperty("events_url")
        val eventsUrl: String = "",

        @field:JsonProperty("html_url")
        val htmlUrl: String = "",

        @field:JsonProperty("site_admin")
        val siteAdmin: Boolean = false,

        @field:JsonProperty("gravatar_id")
        val gravatarId: String = "",

        @field:JsonProperty("organizations_url")
        val organizationsUrl: String = ""
) : Serializable
