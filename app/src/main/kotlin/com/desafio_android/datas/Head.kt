package com.concrete_challenge.datas

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

data class Head(

        @field:JsonProperty("ref")
        val ref: String = "",

        @field:JsonProperty("repo")
        val repo: Repo = Repo(),

        @field:JsonProperty("label")
        val label: String = "",

        @field:JsonProperty("sha")
        val sha: String = "",

        @field:JsonProperty("user")
        val user: User = User()
) : Serializable
