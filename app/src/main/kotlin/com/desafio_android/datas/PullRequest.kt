package com.concrete_challenge.datas

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

data class PullRequest(

        @field:JsonProperty("issue_url")
        val issueUrl: String = "",

        @field:JsonProperty("_links")
        val links: Links = Links(),

        @field:JsonProperty("diff_url")
        val diffUrl: String = "",

        @field:JsonProperty("created_at")
        val createdAt: String = "",

        @field:JsonProperty("assignees")
        val assignees: List<Any?>? = null,

        @field:JsonProperty("requested_reviewers")
        val requestedReviewers: List<Any?>? = null,

        @field:JsonProperty("title")
        val title: String = "",

        @field:JsonProperty("body")
        val body: String = "",

        @field:JsonProperty("head")
        val head: Head = Head(),

        @field:JsonProperty("number")
        val number: Int = 0,

        @field:JsonProperty("patch_url")
        val patchUrl: String = "",

        @field:JsonProperty("updated_at")
        val updatedAt: String = "",

        @field:JsonProperty("merge_commit_sha")
        val mergeCommitSha: String = "",

        @field:JsonProperty("comments_url")
        val commentsUrl: String = "",

        @field:JsonProperty("review_comment_url")
        val reviewCommentUrl: String = "",

        @field:JsonProperty("id")
        val id: Int = 0,

        @field:JsonProperty("state")
        val state: String = "",

        @field:JsonProperty("locked")
        val locked: Boolean = false,

        @field:JsonProperty("commits_url")
        val commitsUrl: String = "",

        @field:JsonProperty("closed_at")
        val closedAt: Any? = null,

        @field:JsonProperty("statuses_url")
        val statusesUrl: String = "",

        @field:JsonProperty("merged_at")
        val mergedAt: Any? = null,

        @field:JsonProperty("url")
        val url: String = "",

        @field:JsonProperty("milestone")
        val milestone: Any? = null,

        @field:JsonProperty("html_url")
        val htmlUrl: String = "",

        @field:JsonProperty("review_comments_url")
        val reviewCommentsUrl: String = "",

        @field:JsonProperty("assignee")
        val assignee: Any? = null,

        @field:JsonProperty("user")
        val user: User = User(),

        @field:JsonProperty("base")
        val base: Base = Base()
) : Serializable
