package com.concrete_challenge.datas

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

data class Comments(

        @field:JsonProperty("href")
        val href: String = ""

) : Serializable
