package com.concrete_challenge.datas

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

data class GitHubResponse(

        @field:JsonProperty("total_count")
        val totalCount: Int = 0,

        @field:JsonProperty("incomplete_results")
        val incompleteResults: Boolean = false,

        @field:JsonProperty("items")
        val items: MutableList<Repository> = mutableListOf()
) : Serializable
