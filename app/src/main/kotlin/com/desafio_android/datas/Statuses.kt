package com.concrete_challenge.datas

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

data class Statuses(

        @field:JsonProperty("href")
        val href: String = ""
) : Serializable
