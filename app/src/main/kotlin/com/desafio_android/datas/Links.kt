package com.concrete_challenge.datas

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

data class Links(

        @field:JsonProperty("comments")
        val comments: Comments = Comments(),

        @field:JsonProperty("issue")
        val issue: Issue = Issue(),

        @field:JsonProperty("self")
        val self: Self = Self(),

        @field:JsonProperty("review_comments")
        val reviewComments: ReviewComments = ReviewComments(),

        @field:JsonProperty("commits")
        val commits: Commits = Commits(),

        @field:JsonProperty("statuses")
        val statuses: Statuses = Statuses(),

        @field:JsonProperty("html")
        val html: Html = Html(),

        @field:JsonProperty("review_comment")
        val reviewComment: ReviewComment = ReviewComment()
) : Serializable
