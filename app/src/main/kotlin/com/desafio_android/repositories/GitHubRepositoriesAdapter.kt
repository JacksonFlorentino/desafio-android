package com.concrete_challenge.repositories

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.concrete_challenge.R
import com.concrete_challenge.datas.Repository
import com.concrete_challenge.helpers.downloadImage
import com.concrete_challenge.helpers.isBeforePenultimatePosition
import com.concrete_challenge.repositories.GitHubRepositoriesAdapter.ViewHolder
import com.concrete_challenge.repositories.GitHubRepositoriesContract.Presenter

/**
 * Created by jackmiras on 7/3/17.
 */
class GitHubRepositoriesAdapter(val presenter: Presenter, val dataset: MutableList<Repository>) : Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent!!.context)
        val view = layoutInflater.inflate(R.layout.list_item_repository, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repository = dataset[position]
        holder.textViewName.text = repository.name
        holder.textViewDescription.text = repository.description
        holder.textViewForks.text = repository.forks.toString()
        holder.textViewStars.text = repository.stargazersCount.toString()
        holder.textViewUserName.text = repository.owner.login
        holder.imageViewAvatar.downloadImage(repository.owner.avatarUrl, holder.progressBar, R.drawable.ic_profile_gray_48dp)
        holder.container.setOnClickListener { _ -> presenter.onItemClicked(repository) }
        if (dataset.isBeforePenultimatePosition(repository)) presenter.fetchRepositories()
    }

    fun addAll(repositories: MutableList<Repository>) {
        dataset.addAll(repositories)
        notifyDataSetChanged()
    }

    override fun getItemCount() = dataset.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val container: View by lazy { view.findViewById(R.id.linearlayout_container) }
        val textViewName: TextView by lazy { view.findViewById(R.id.textview_name) as TextView }
        val textViewDescription: TextView by lazy { view.findViewById(R.id.textview_description) as TextView }
        val textViewForks: TextView by lazy { view.findViewById(R.id.textview_forks) as TextView }
        val textViewStars: TextView by lazy { view.findViewById(R.id.textview_stars) as TextView }
        val imageViewAvatar: ImageView by lazy { view.findViewById(R.id.imageview_avatar) as ImageView }
        val progressBar: ProgressBar by lazy { view.findViewById(R.id.progressbar_avatar) as ProgressBar }
        val textViewUserName: TextView by lazy { view.findViewById(R.id.textview_username) as TextView }
    }
}