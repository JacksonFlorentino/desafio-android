package com.desafio_android.repositories

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.concrete_challenge.datas.Repository
import com.concrete_challenge.repositories.GitHubRepositoriesAdapter
import com.concrete_challenge.repositories.GitHubRepositoriesContract
import com.concrete_challenge.repositories.GitHubRepositoriesPresenter
import com.example.jackmiras.placeholderj.library.PlaceHolderJ
import kotlinx.android.synthetic.main.activity_repositories.*
import kotlinx.android.synthetic.main.activity_repositories.recyclerview_repositories as recyclerViewRepositories


/**
 * Created by jackmiras on 7/2/17.
 */
class GitHubRepositoriesActivity : AppCompatActivity(), GitHubRepositoriesContract.View {

    var gitHubRepositoriesAdapter: GitHubRepositoriesAdapter? = null
    val presenter by lazy { GitHubRepositoriesPresenter(this) }
    val placeHolderJ by lazy { initPlaceHolderJ() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repositories)

        toolbar.setTitle(R.string.activity_repositories_label)
        setSupportActionBar(toolbar)

        presenter.fetchRepositories()
    }

    private fun initPlaceHolderJ(): PlaceHolderJ {
        val placeHolderJ = PlaceHolderJ(this, R.id.recyclerview_repositories)
        placeHolderJ.init(R.id.view_loading, R.id.view_empty, R.id.view_error)
        return placeHolderJ
    }

    override fun showRepositories(repositories: MutableList<Repository>) {
        if (gitHubRepositoriesAdapter == null) {
            gitHubRepositoriesAdapter = GitHubRepositoriesAdapter(presenter, repositories)
            recyclerViewRepositories.layoutManager = LinearLayoutManager(this)
            recyclerViewRepositories.adapter = gitHubRepositoriesAdapter
        } else {
            gitHubRepositoriesAdapter?.addAll(repositories)
        }
    }

    override fun showPullRequests(repository: Repository) {
        val intent = Intent(this, PullRequestsActivity::class.java)
        intent.putExtra(Constants.REPOSITORY_NAME, repository.name)
        intent.putExtra(Constants.USERNAME, repository.owner.login)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    override fun showScreenContent() = placeHolderJ.showContent()

    override fun showLoadingView() = placeHolderJ.showLoading()

    override fun showEmptyList() = placeHolderJ.showEmpty({ _ -> presenter.fetchRepositories() })

    override fun showError(t: Throwable) = placeHolderJ.showError(t, { presenter.fetchRepositories(isNetWorkOnline()) })
}