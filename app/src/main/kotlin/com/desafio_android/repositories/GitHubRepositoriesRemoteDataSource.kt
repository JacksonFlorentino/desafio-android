package com.concrete_challenge.repositories

import com.concrete_challenge.datas.GitHubResponse
import com.concrete_challenge.network.ApiClient
import com.concrete_challenge.repositories.GitHubRepositoriesContract.RepositoryActions
import com.concrete_challenge.repositories.GitHubRepositoriesContract.RepositoryCallback
import com.desafio_android.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jackmiras on 7/4/17.
 */
class GitHubRepositoriesRepository : RepositoryActions {

    override fun fetchRepositories(page: Int, perPage: Int, repositoryCallback: RepositoryCallback) {
        //TODO: Change this for a RxKotlin2 Observable.
        val call = ApiClient().apiService.fetchRepositories(page, perPage)
        call.enqueue(object : Callback<GitHubResponse> {

            override fun onFailure(call: Call<GitHubResponse>?, t: Throwable?) {
                val error = t ?: Throwable("Retrofit has thrown an unknown error")
                repositoryCallback.onFailure(error)
            }

            override fun onResponse(call: Call<GitHubResponse>?, response: Response<GitHubResponse>?) {
                if (response == null || response.body() == null) {
                    onFailure(call, Throwable("Retrofit has returned a null Response object"))
                } else {
                    repositoryCallback.onResponse((response.body() as GitHubResponse).items)
                }
            }
        })
    }
}