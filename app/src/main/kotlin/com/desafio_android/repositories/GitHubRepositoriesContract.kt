package com.concrete_challenge.repositories

import com.concrete_challenge.datas.Repository

/**
 * Created by jackmiras on 7/2/17.
 */
interface GitHubRepositoriesContract {

    interface View {

        fun showScreenContent()

        fun showLoadingView()

        fun showEmptyList()

        fun showError(t: Throwable)

        fun showRepositories(repositories: MutableList<Repository>)

        fun showPullRequests(repository: Repository)
    }

    interface Presenter {

        fun fetchRepositories(isNetWorkOnline: Boolean)

        fun onItemClicked(repository: Repository)

    }
}